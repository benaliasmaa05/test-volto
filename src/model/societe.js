export default class Societe {
  constructor(data) {
    this.id = data.id;
    this.siret = data.siret;
    this.RaisonSocial = data.RaisonSocial;
    this.compteurs = [];
    this.numCompteur = null;
    this.dateDebut = null;
    this.dateFin = null;
  }
  static create(data) {
    return new Societe(data);
  }
}
