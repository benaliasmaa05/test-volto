import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "index",
    component: () =>
      import(/* webpackChunkName: "index" */ "../views/Login/index.vue"),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("token")) {
        next("/");
      } else {
        next();
      }
    },
    children: [
      {
        path: "",
        name: "Login",
        component: () =>
          import(/* webpackChunkName: "Login" */ "../views/Login/Login.vue"),
      },
      {
        path: "/register",
        name: "Register",
        component: () =>
          import(
            /* webpackChunkName: "Register" */ "../views/Login/Register.vue"
          ),
      },
    ],
  },
  {
    path: "/",
    name: "Home",
    component: () => import(/* webpackChunkName: "Home" */ "../views/Home.vue"),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem("token")) {
        next();
      } else {
        next("/login");
      }
    },
    children: [
      {
        path : '/' , 
        name : "Logo" , 
        component: () =>
        import(
          /* webpackChunkName: "Logo" */ "../views/Logo.vue"
        ),
      },
      {
        path: "/societe",
        name: "Societe",
        component: () =>
          import(
            /* webpackChunkName: "Societe" */ "../views/Societe/index.vue"
          ),
      },
      {
        path: "/Calcul",
        name: "Calcul",
        component: () =>
          import(/* webpackChunkName: "Calcul" */ "../views/Calcul/index.vue"),
      },
      {
        path: "/historique-calcul",
        name: "Historique",
        component: () =>
          import(
            /* webpackChunkName: "Historique" */ "../views/historique.vue"
          ),
      },
    ],
  },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
