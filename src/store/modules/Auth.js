import axios from "axios";
import router from "@/router";
const state = {
  user: null,
};
const getters = {
  user: (state) => state.user,
};
const mutations = {
  SET_USER_AUTH(state, payload) {
    state.user = payload;
  },
};
const actions = {
  async createCompte({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append(
        "token",
        "jhgjsknbhjguskl5554df464dg4fjghf54sd5d6df7468f7hg5f4gvd46cs7s8fth64g6fd788654cs6r7f87fd64s8r67f"
      );
      formData.append("nom", payload?.nom);
      formData.append("prenom", payload?.prenom);
      formData.append("email", payload?.email);
      formData.append("password", payload?.password);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/createCompte/",
        formData,
      );
      return { response: response.data.data };
    } catch (error) {
      return false;
    }
  },
  async login({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append("email", payload?.email);
      formData.append("password", payload?.password);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/login/",
        formData,   
      );
      if (response?.data?.login == true) {
        localStorage.setItem("token", response.data.token);
        localStorage.setItem("User-Details", JSON.stringify(response.data));
        commit("SET_USER_AUTH", response.data);
        router.push("/");
        return true ;
      }else {
        return { response: response.data };

      }
    } catch (error) {
      return false;
    }
  },
};
export default {
  state,
  getters,
  mutations,
  actions,
};
