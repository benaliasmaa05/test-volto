import axios from "axios";
const state = {
  compteurs: [],
  errorCompteurs: [],
};
const getters = {
  compteurs: (state) => state.compteurs,
  errorCompteurs: (state) => state.errorCompteurs,
};
const mutations = {
  SET_ALL_COMPTEURS(state, payload) {
    state.compteurs = payload;
  },
  SET_ERROR_COMPTEURS(state, payload) {
    state.errorCompteurs = payload;
  },
};
const actions = {
  async getListeCompteurParSociete({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getListeCompteurParSociete/",
        formData
      );
      if (response.data.error == false) {
        commit("SET_ALL_COMPTEURS", response.data.data);
      } else {
        commit("SET_ERROR_COMPTEURS", response.data.type);
      }
      return true;
    } catch (error) {
      return false;
    }
  },
  async getListeCompteurParSocieteCalcul({ commit }, payload) {
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload.siret);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getListeCompteurParSociete/",
        formData
      );
      if (response.data.error == false) {
        payload.compteurs = response.data.data;
      } else {
        commit("SET_ERROR_COMPTEURS", response.data.type);
      }
      return true;
    } catch (error) {
      return false;
    }
  },
  async ajouterCompteurToSociete({ commit }, payload) {
    commit("SET_ERROR_COMPTEURS", null);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload?.siret);
      formData.append("numCompteur", payload?.numCompteur);
      formData.append("consommation", payload?.consommation);
      formData.append("typeEnergie", payload?.typeEnergie);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/ajouterCompteurToSociete/",
        formData
      );
      if (response.data.error == false) {
        return true;
      } else {
        commit("SET_ERROR_COMPTEURS", response.data.type);
        return false;
      }
    } catch (error) {
      return false;
    }
  },
  async getCompteurParSociete({ commit }, payload) {
    commit("SET_ERROR_COMPTEURS", null);
    try {
      const formData = new FormData();
      formData.append("token", localStorage.getItem("token"));
      formData.append("siret", payload.siret);
      formData.append("numCompteur", payload.numCompteur);
      const response = await axios.post(
        "https://testbackend.smart-electricite.com/api/getCompteurParSociete/",
        formData
      );
      if (response.data.error == false) {
        commit("SET_ALL_COMPTEURS", [response.data.data]);
      } else {
        commit("SET_ERROR_COMPTEURS", response.data.type);
      }
    } catch (error) {
      return false;
    }
  },
  resetErrorCompteur({ commit }) {
    commit("SET_ERROR_COMPTEURS", null);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
